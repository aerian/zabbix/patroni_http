# Patroni by HTTP

## Overview

For Zabbix version: 6.2 and higher  
The template to monitor Patroni by Zabbix that work without any external scripts.
Most of the metrics are collected in one go, thanks to Zabbix bulk data collection.

Template `Patroni`  collects metrics by HTTP agent from /patroni endpoint.
See https://patroni.readthedocs.io/en/latest/rest_api.html.

This template was tested on:

- Patroni, version 2.1+

## Setup

> See [Zabbix template operation](https://www.zabbix.com/documentation/6.2/manual/config/templates_out_of_the_box/http) for basic instructions.

1. Import template into Zabbix
2. After importing template make sure that patroni allows for metric collection.
  Test by running: `curl -L http://localhost:8008/patroni`
3. Check if patroni is accessible from Zabbix proxy or Zabbix server depending on where you are planning to do the monitoring.
  To verify run `curl -L  http://<patroni_node_address>:8008/patroni`
4. Add the template to each node with patroni.
  By default template use client port. 

  If you have specified a non-standard port for patroni, don't forget change macros {$PATRONI.SCHEME}, {$PATRONI.PORT}.

  If you need it, you can set {$PATRONI.USERNAME} and {$PATRONI.PASSWORD} macros in the template for using on the host level.

  Test availability: `zabbix_get -s patroni-host -k patroni.state`

Besides, see the macros section as it will set the trigger values.


## Zabbix configuration

No specific Zabbix configuration is required.

`Doc in progress`